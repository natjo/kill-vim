# 1001 Ways to Kill Vim
## Casual
```
# Normal mode
:q
:wq
;q!
```

## Expert
```
# Normal mode
ZZ
ZQ
```

## Timeout
```
timeout 10 vim
timeout $RANDOM vim
timeout $(echo "$RANDOM / 1000" | bc) vim
# Check timeout with
ps a | grep vim | grep -v grep
```

## Kill from another terminal
```
ps a | grep vim | grep -v grep | tr -s ' ' | cut -d ' ' -f2 | xargs kill -9
```

## Kill via alias
Reload terminal with `source $HOME/.zshrc`
```
alias kill-vim='ps a | grep vim | grep -v grep | tr -s '\'' '\'' | cut -d '\'' '\'' -f2 | xargs kill -9'
```

## Kill from remote repo
```
curl -s https://gitlab.com/natjo/kill-vim/raw/main/script.sh | bash
```

## Python server
Setup DNS entry at afraid.org: `terminate-jonas-vim.mooo.com -> 192.168.X.Y`

```python
from http.server import HTTPServer, BaseHTTPRequestHandler
from os import system


class S(BaseHTTPRequestHandler):
    def do_GET(self):
        system('killall -9 vim')
        self.send_response(200)
        self.end_headers()
        self.wfile.write("You just killed vim!".encode("utf8"))

if __name__ == "__main__":
    HTTPServer(("0.0.0.0", 8080), S).serve_forever()
```

## A lot more ideas
You can find a lot more ideas [here](https://github.com/hakluke/how-to-exit-vim)
