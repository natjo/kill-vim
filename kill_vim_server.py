#!/bin/python3

from http.server import HTTPServer, BaseHTTPRequestHandler
from os import system


class S(BaseHTTPRequestHandler):
    def do_GET(self):
        system('killall -9 vim')
        self.send_response(200)
        self.end_headers()
        self.wfile.write("You just killed vim!".encode("utf8"))

if __name__ == "__main__":
    HTTPServer(("0.0.0.0", 8080), S).serve_forever()

