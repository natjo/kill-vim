#!/bin/bash

# Warn before killing
echo -n "Killing vim"
sleep 1
echo -n "."
sleep 1
echo -n "."
sleep 1
echo -n "."
echo ""
sleep 1

# Kill vim, ignore error messages
ps a | grep vim | grep -v grep | tr -s ' ' | cut -d ' ' -f1 | xargs kill -9 > /dev/null 2>&1

if [[ $? -eq 0 ]]; then
    echo "vim killed successfully"
else
    echo "Failed to kill vim"
fi
